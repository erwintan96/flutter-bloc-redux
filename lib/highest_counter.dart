import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

class HighestCounter extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Highest Counter"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Current Highest Counter:',
            ),
            StoreConnector<int, String>(converter: (store) {
              return store.state.toString();
            }, builder: (context, state) {
              return Text(
                '$state',
                style: Theme.of(context).textTheme.display1,
              );
            }),
          ],
        ),
      ),
    );
  }
}
