import 'package:flutter_bloc_redux_sample/redux/actions.dart';

int reducer(int state, dynamic action) {
  if (action is StoreAction) {
    return action.value;
  } else {
    return 0;
  }
}